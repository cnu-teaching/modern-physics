const ec = new EventCalendar(document.getElementById('ec'), {
    view: 'dayGridMonth',
    headerToolbar: {
        start: 'prev,next today',
        center: 'title',
        end: 'dayGridMonth,timeGridWeek,listWeek'
    },
    hiddenDays: [ 0, 1, 2, 3, 6 ],
    scrollTime: '09:00:00',
    events: createEvents(),
    height: '800px',
    dayMaxEvents: true,
    nowIndicator: true,
    selectable: true,
    editable: false,
    eventStartEditable: false,
    eventDurationEditable: false
});

function createEvents() {
    return [
        {start: "2024-03-07 16:00:00", end: "2024-03-07 17:00:00", title: "강의 소개", color: "#83a598"},
        {start: "2024-03-14 16:00:00", end: "2024-03-14 17:00:00", title: "퀴즈와 과제 제출", color: "#83a598"},
        {start: "2024-03-21 16:00:00", end: "2024-03-21 17:00:00", title: "퀴즈와 과제 제출", color: "#83a598"},
        {start: "2024-03-28 16:00:00", end: "2024-03-28 17:00:00", title: "퀴즈와 과제 제출", color: "#83a598"},
        {start: "2024-04-04 16:00:00", end: "2024-04-04 17:00:00", title: "퀴즈와 과제 제출", color: "#83a598"},
        {start: "2024-04-11 16:00:00", end: "2024-04-11 17:00:00", title: "강의", color: "#83a598"},
        {start: "2024-04-18 16:00:00", end: "2024-04-18 17:00:00", title: "중간시험 (과제 제출)", color: "#83a598"},
        {start: "2024-04-25 16:00:00", end: "2024-04-25 17:00:00", title: "휴강", color: "#83a598"},
        {start: "2024-05-02 16:00:00", end: "2024-05-02 17:00:00", title: "퀴즈와 과제 제출", color: "#83a598"},
        {start: "2024-05-09 16:00:00", end: "2024-05-09 17:00:00", title: "휴강 (채전)", color: "#83a598"},
        {start: "2024-05-16 16:00:00", end: "2024-05-16 17:00:00", title: "퀴즈와 과제 제출", color: "#83a598"},
        {start: "2024-05-23 16:00:00", end: "2024-05-23 17:00:00", title: "퀴즈와 과제 제출", color: "#83a598"},
        {start: "2024-05-30 16:00:00", end: "2024-05-30 17:00:00", title: "강의+퀴즈+과제", color: "#83a598"},
        {start: "2024-06-20 16:00:00", end: "2024-06-20 17:00:00", title: "기말시혐", color: "#83a598"},
        //
        {start: "2024-03-08 16:00:00", end: "2024-03-08 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-03-15 16:00:00", end: "2024-03-15 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-03-22 16:00:00", end: "2024-03-22 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-03-29 16:00:00", end: "2024-03-29 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-04-05 16:00:00", end: "2024-04-05 18:00:00", title: "휴강", color: "#83a598"},
        {start: "2024-04-12 16:00:00", end: "2024-04-12 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-04-19 16:00:00", end: "2024-04-19 18:00:00", title: "휴강", color: "#83a598"},
        {start: "2024-04-26 16:00:00", end: "2024-04-26 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-05-03 16:00:00", end: "2024-05-03 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-05-10 16:00:00", end: "2024-05-10 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-05-17 16:00:00", end: "2024-05-17 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-05-24 16:00:00", end: "2024-05-24 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-05-31 16:00:00", end: "2024-05-31 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-06-07 16:00:00", end: "2024-06-07 18:00:00", title: "강의", color: "#83a598"},
        {start: "2024-06-07 18:00:00", end: "2024-06-07 19:00:00", title: "강의 (6/6 보강)", color: "#83a598"},
        {start: "2024-06-21 16:00:00", end: "2024-06-21 18:00:00", title: "휴강", color: "#83a598"},
        //
        {start: "2024-04-10", end: "2024-04-10", allDay: true, color: "#cc241d", title: "휴교 (국회의원 선거)"},
        {start: "2024-05-01", end: "2024-05-01", allDay: true, color: "#cc241d", title: "휴교 (근로자의 날)"},
        {start: "2024-05-06", end: "2024-05-06", allDay: true, color: "#cc241d", title: "휴교 (어린이날 대체휴일)"},
        {start: "2024-05-15", end: "2024-05-15", allDay: true, color: "#cc241d", title: "휴교 (부처님 오신날)"},
        {start: "2024-06-06", end: "2024-06-06", allDay: true, color: "#cc241d", title: "휴교 (현충일)"},
    ];
};
