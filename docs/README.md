# Modern Physics

## 수업운영방식

- 이론강의 3시간 - 교재를 바탕으로 한 준비된 슬라이드를 설명해주는 형식
- 매주 퀴즈 - 퀴즈는 매주 지난 시간 내용을 바탕으로 복습 정도를 평가
- 연습문제 풀이 발표 - 매주 한명씩 무작위로 골라 과제 풀이 발표

- [강의 일정표](./schedule) (휴강, 보강, 시험 등)

## 시험일정

### 중간시험

- 범위: 4월 12일 강의 내용 까지
- 날짜: 4월 18일

### 기말시험

- 범위: 중간시험 범위 이후부터
- 날짜: 6월 20일

<div>
채점 답안 다운로드:
<form action="http://0.0.0.0:8080/download.php?course=modern-physics" method="POST">
ID: <input type="text" name="student_id" style="border: 2px solid red;">
<input type="submit">
</form>
</div>

## 공지사항

- 수업에 대한 모든 공지는 수업시간, 이메일 또는 강의 홈페이지 (https://cnu-teaching.gitlab.io/modern-physics/)를 통해 이루어짐

    > 통합정보 이메일 주소 업데이트 중요

- 진도, 학습 평가 등은 별도의 이야기가 없으면 강의 홈페이지에 기술된 내용을 따름
- 퀴즈, 중간, 기말 시험 채점 결과는 [공유폴더](https://cnu365-my.sharepoint.com/:f:/g/personal/kmin_o_cnu_ac_kr/EsEi6xYyi59GhXZmPbB1_f0BytcQ8EdOPguA0J3jGb7bsw?e=bgafMG)에 업로드

## 학습평가

- 중간 시험 - 35%
- 기말 시험 - 35%
- 매주 퀴즈 - 25%
- 과제 제출 - 5% (연습문제 풀이는 **A4** 로 제출한 것만 받음)

## 학습과제

- 그 주 강의내용과 관련있는 교제의 연습문제 중 2 문제를 골라 풀어서 제출.
- <ins>**과제 풀이 제출은 6장 연습문제 까지만**</ins>

## Acknowledgment

Lecture schedule powered by [Event Calendar](https://github.com/vkurko/calendar)
