#!/bin/sh
#
# Copyright (c) 2023-2024, Kyungguk Min
#
# SPDX-License-Identifier: BSD-2-Clause
#

# Set site directory
SITE_DIR="$1"
if [ -z "${SITE_DIR}" ]; then
    SITE_DIR=public
fi
echo "Site Directory : ${SITE_DIR}"

# Build the site
mkdocs build --strict --verbose --config-file mkdocs.yml --site-dir "${SITE_DIR}" || exit 1
