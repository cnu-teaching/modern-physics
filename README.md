# Modern Physics Lecture Note

[![pipeline status](https://gitlab.com/cnu-teaching/modern-physics/badges/master/pipeline.svg)](https://gitlab.com/cnu-teaching/modern-physics/-/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

The lecture schedule calendar is powered by [Event Calendar](https://github.com/vkurko/calendar).
